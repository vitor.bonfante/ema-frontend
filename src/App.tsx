import React, { useEffect, useState } from 'react';
import { ThemeProvider } from 'styled-components';
import { Header } from './components/Header';
import { Search } from './components/Search';
import { GlobalStyles } from './styles/GlobalStyles';
import { lightTheme, darkTheme } from './styles/Themes';

const App = () => {
  const [theme, setTheme] = useState<string>();

  useEffect(() => {
    const theme = localStorage.getItem('theme');

    if (theme) {
      setTheme(theme);
    } else {
      setTheme('light');
      localStorage.setItem('theme', 'light');
    }
  }, []);

  function changeTheme() {
    if (theme === 'light') {
      setTheme('dark');
      localStorage.setItem('theme', 'dark');
    } else {
      setTheme('light');
      localStorage.setItem('theme', 'light');
    }
  }

  return (
    <ThemeProvider theme={theme === 'dark' ? darkTheme : lightTheme}>
      <React.Fragment>
        <Header theme={theme === 'dark'} changeTheme={changeTheme} />
        <Search />
        <GlobalStyles />
      </React.Fragment>
    </ThemeProvider>
  );
};

export default App;
