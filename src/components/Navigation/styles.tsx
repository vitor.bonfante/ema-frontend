import styled from 'styled-components';

export const NavigationContainer = styled.div`
  display: flex;
  align-items: center;

  > h5 {
    margin-left: 6px;
    margin-right: 6px;
  }
`;
