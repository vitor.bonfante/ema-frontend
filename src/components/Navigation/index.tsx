import React, { MouseEventHandler } from 'react';
import { AiOutlineArrowLeft, AiOutlineArrowRight } from 'react-icons/ai';

import { NavigationContainer } from './styles';

interface NavigationProps {
  nextPage: MouseEventHandler<SVGElement>;
  previousPage: MouseEventHandler<SVGElement>;
  page: number;
}

const Navigation = (props: NavigationProps) => {
  return (
    <NavigationContainer>
      <AiOutlineArrowLeft cursor='pointer' onClick={props.previousPage} />
      <h5>Página: {props.page}</h5>
      <AiOutlineArrowRight cursor='pointer' onClick={props.nextPage} />
    </NavigationContainer>
  );
};

export { Navigation };
