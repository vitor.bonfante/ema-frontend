import { AxiosInstance } from 'axios';
import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { IoMdClose } from 'react-icons/io';
import { Loading } from '../Loading';

import { ExitContainer, ModalBackground, ModalContent } from './styles';

interface ModalProps {
  isOpen: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  selectedUser: string;
  api: AxiosInstance;
}

interface Repo {
  name: string;
  url: string;
}

const ReposModal = (props: ModalProps) => {
  const [loading, setLoading] = useState<boolean>(true);

  const [bestRated, setBestRated] = useState<Repo[]>([]);
  const [lastCreated, setLastCreated] = useState<Repo[]>([]);

  useEffect(() => {
    if (props.selectedUser) {
      getUserRepos();
    }
  }, [props.selectedUser]);

  async function getUserRepos() {
    setLoading(true);
    await props.api
      .get(
        `http://localhost:8080/api/repos/search?userName=${props.selectedUser}`,
      )
      .then(res => {
        console.log(res.data);
        setBestRated(res.data.bestRated);
        setLastCreated(res.data.lastCreated);

        if (res.data.length < 1) {
          alert('Sua busca não trouxe nenhum resultado');
        }
        setLoading(false);
      })
      .catch(err => {
        alert('Houve um problema na busca, tente novamente');
        setLoading(false);
      });

    setLoading(false);
  }

  return props.isOpen ? (
    <ModalBackground>
      <ModalContent>
        <ExitContainer>
          <IoMdClose
            cursor='pointer'
            onClick={() => {
              setBestRated([]);
              setLastCreated([]);
              props.setOpen(false);
            }}
          />
        </ExitContainer>
        {loading ? (
          <Loading />
        ) : (
          <>
            <h4>Últimos Repositórios</h4>
            <ul>
              {lastCreated.map(repo => {
                return (
                  <li>
                    <a href={repo.url} target='_blank'>
                      {repo.name}
                    </a>
                  </li>
                );
              })}
            </ul>

            <h4>Melhores Repositórios</h4>
            <ul>
              {bestRated.map(repo => {
                return (
                  <li>
                    <a href={repo.url} target='_blank'>
                      {repo.name}
                    </a>
                  </li>
                );
              })}
            </ul>
          </>
        )}
      </ModalContent>
    </ModalBackground>
  ) : null;
};

export { ReposModal };
