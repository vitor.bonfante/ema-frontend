import styled from 'styled-components';

export const ModalBackground = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(128, 128, 128, 0.6);
  display: flex;
  flex-direction: column;
  z-index: 100;
  top: 0;
  left: 0;

  align-items: center;

  padding-bottom: 20px;
  position: fixed;
  overflow-y: scroll;
  padding-top: 20px;

  scrollbar-width: none;
  scrollbar-color: transparent;
  ::-webkit-scrollbar {
    width: 0;
    background: transparent;
  }
`;

export const ExitContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 8px;
`;

export const ModalContent = styled.div`
  width: 460px;
  padding: 24px 30px;
  background-color: var(--white);
  border-radius: 10px;
  z-index: 101;

  > ul {
    margin-bottom: 12px;
  }

  a {
    text-decoration: none !important;
    color: var(--border);

    :hover {
      color: var(--purple);
      transition: 0.25s;
    }
  }

  @media only screen and (max-width: 768px) {
    width: 100vw;
    min-height: 100vh;
    border-radius: 0px;
    overflow-y: scroll;
  }

  @media (max-width: 400px) {
    padding: 24px 18px;
  }
`;
