import styled from 'styled-components';

export const Main = styled.main`
  width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Card = styled.div`
  border: 3px solid var(--border);
  cursor: pointer;
  border-radius: 6px;
  margin-top: 12px;
  padding: 12px;
  width: fit-content;
  display: flex;
  > img {
    width: 100px;
    height: 100px;
    border: 1px solid var(--border);
  }
`;

export const Texts = styled.div`
  margin-left: 12px;
  width: 100%;
  > p {
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    display: flex;
  }
`;

export const InputContainer = styled.div`
  margin-top: 12px;
  margin-bottom: 24px;
  > input {
    padding: 12px;
    outline: none;
    border-radius: 3px 0px 0px 3px;

    background-color: var(--input);
    color: var(--black);
    border: 2px solid var(--border);
  }

  > button {
    background-color: var(--border);
    color: var(--white);
    cursor: pointer;
    padding-left: 12px;
    padding-right: 12px;
    outline: none;
    border: none;
    height: 100%;
    border-radius: 0px 3px 3px 0px;
    > p {
      color: #fff;
    }
  }
`;
