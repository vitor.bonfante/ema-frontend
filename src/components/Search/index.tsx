import React, { FormEvent, useState } from 'react';
import axios from 'axios';

import { Card, InputContainer, Main, Texts } from './styles';
import { Loading } from '../Loading';
import { ReposModal } from '../ReposModal';
import { Navigation } from '../Navigation';

interface UserProps {
  avatar: string;
  userName: string;
  followersCount: number;
  repositoriesCount: number;
}

const AXIOS_URL = 'http://localhost:8080/api';
const api = axios.create({
  baseURL: AXIOS_URL,
});

const Search = () => {
  const [users, setUsers] = useState<UserProps[]>([]);
  const [searchInput, setSearchInput] = useState<string>();
  const [loading, setLoading] = useState<boolean>(false);
  const [reposModalOpen, setReposModalOpen] = useState<boolean>(false);
  const [selectedUser, setSelectedUser] = useState<string>('');
  const [page, setPage] = useState<number>(1);

  async function searchUsername(page: number) {
    if (!searchInput) {
      return alert('Digite algo antes de buscar!');
    }
    setLoading(true);

    await api
      .get(
        `http://localhost:8080/api/user/search?searchUserName=${searchInput}&page=${page}`,
      )
      .then(res => {
        console.log(res.data);
        setUsers(res.data);
        if (res.data.length < 1) {
          alert('Sua busca não trouxe nenhum resultado');
        }
        setLoading(false);
      })
      .catch(err => {
        alert('Houve um problema na busca, tente novamente');
        setLoading(false);
      });
  }

  return (
    <Main>
      <ReposModal
        api={api}
        isOpen={reposModalOpen}
        setOpen={setReposModalOpen}
        selectedUser={selectedUser}
      />
      <InputContainer>
        <input
          type='text'
          placeholder='Insira um nome de usuário'
          value={searchInput}
          onChange={evt => {
            setSearchInput(evt.target.value);
          }}
        />
        <button
          onClick={() => {
            setPage(1);
            searchUsername(1);
          }}
        >
          <p>Buscar</p>
        </button>
      </InputContainer>

      {loading ? (
        <Loading />
      ) : (
        <>
          {users.length > 0 && (
            <Navigation
              page={page}
              previousPage={() => {
                if (page > 1) {
                  setPage(page - 1);
                  searchUsername(page - 1);
                }
              }}
              nextPage={() => {
                setPage(page + 1);
                searchUsername(page + 1);
              }}
            />
          )}

          {users.map((user, i) => {
            return (
              <Card
                key={i}
                onClick={() => {
                  setSelectedUser(user.userName);
                  setReposModalOpen(true);
                }}
              >
                <img src={user.avatar} alt='' />
                <Texts>
                  <p>
                    Nome de usuário:<h4>{user.userName}</h4>
                  </p>
                  <p>
                    Qnt. seguidores: <h4>{user.followersCount}</h4>
                  </p>
                  <p>
                    Qnt. repositórios: <h4>{user.repositoriesCount}</h4>
                  </p>
                </Texts>
              </Card>
            );
          })}
        </>
      )}
    </Main>
  );
};

export { Search };
