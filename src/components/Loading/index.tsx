import React from 'react';
import { Loader } from './styles';

const Loading = () => {
  return <Loader />;
};

export { Loading };
