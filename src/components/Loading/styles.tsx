import styled from 'styled-components';

export const Loader = styled.div`
  margin: auto;
  border: 8px solid var(--border);
  border-radius: 50%;
  border-top: 8px solid var(--purple);
  width: 50px;
  height: 50px;
  animation: spinner 1s linear infinite;

  @keyframes spinner {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
