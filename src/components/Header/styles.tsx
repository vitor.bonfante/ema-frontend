import styled from 'styled-components';

export const StyledHeader = styled.header`
  width: 100vw;
  background-color: var(--header);
  color: #fff;
  display: flex;
  justify-content: space-between;
  padding: 16px;
`;

export const DarkModeContainer = styled.div`
  display: flex;
  color: #fff;
  align-items: center;

  > em {
    margin-right: 12px;
  }
`;
