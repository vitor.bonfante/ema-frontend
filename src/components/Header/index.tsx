import React, { useEffect, useState } from 'react';
import Switch from 'react-switch';

import { DarkModeContainer, StyledHeader } from './styles';

interface HeaderProps {
  theme: boolean;
  changeTheme: any;
}

const Header = (props: HeaderProps) => {
  return (
    <StyledHeader>
      <h1>GitHub Search</h1>
      <DarkModeContainer>
        <em>Dark Mode</em>
        <Switch
          checked={props.theme}
          onChange={props.changeTheme}
          onColor='#988bc7'
          onHandleColor='#483c67'
          boxShadow='0px 1px 5px rgba(0, 0, 0, 0.6)'
          activeBoxShadow='0px 0px 1px 10px rgba(0, 0, 0, 0.2)'
          handleDiameter={30}
          uncheckedIcon={false}
          checkedIcon={false}
          height={20}
          width={48}
        />
      </DarkModeContainer>
    </StyledHeader>
  );
};

export { Header };
