import { css } from 'styled-components';

const Variables = css`
  :root {
    // Font
    --font-main: 'Poppins', -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;

    // Colors
    --header: ${({ theme }) => theme.header};
    --background: ${({ theme }) => theme.background};
    --border: ${({ theme }) => theme.border};
    --purple: ${({ theme }) => theme.purple};
    --white: ${({ theme }) => theme.white};
    --black: ${({ theme }) => theme.black};
    --input: ${({ theme }) => theme.input};
  }
`;

export default Variables;
