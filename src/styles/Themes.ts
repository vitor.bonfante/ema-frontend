export const lightTheme = {
  header: '#191622',
  background: '#fff',
  border: '#483c67',
  purple: '#988bc7',
  white: '#fff',
  black: '#000',
  input: '#fff',
};
export const darkTheme = {
  header: '#191622',
  background: '#000',
  border: '#483c67',
  purple: '#988bc7',
  white: '#000',
  black: '#fff',
  input: '#191622',
};
